<?php

function bpsn_dark_swap_styles() {
	ossn_unload_css('ossn.default');
	ossn_new_css('ossn.default', 'css/bpsn_dark');
  ossn_load_css('ossn.default');
  ossn_add_hook('page', 'load', 'bpsn_inject_page_id');
}

function bpsn_inject_page_id($page_, $loaded_, $html, $params) {
  return str_replace(
    '<body',
    sprintf('<body class="h_bpsn_%s"', $params['handler']),
    $html
  );
}

ossn_register_callback('ossn', 'init', 'bpsn_dark_swap_styles');

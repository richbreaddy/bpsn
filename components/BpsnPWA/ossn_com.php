<?php

define('BSPN_PWA_BASE_URL', ossn_site_url().'components/BpsnPWA');

function bpsn_pwa() {
	ossn_extend_view('ossn/site/head', function () {
		return sprintf(
			'<link rel="manifest" href="%s/manifest.json"/>',
			BSPN_PWA_BASE_URL
		);
	});

	// register service worker
	ossn_extend_view('ossn/site/head', function () {
		$url = BSPN_PWA_BASE_URL;
		return <<<EOD
<script>
if ("serviceWorker" in navigator) {
window.addEventListener("load", () => {
navigator.serviceWorker.register("{$url}/sw.js");
});
}
</script>
EOD;
	});
}

ossn_register_callback('ossn', 'init', 'bpsn_pwa');

<?php

define('SITE_URL_LOGIN', ossn_site_url('login'));

function disable_cr_acc_component() {
	ossn_add_hook('user', 'create', 'disable_non_admin_create_account');
	ossn_add_hook('page', 'override:view', 'disable_create_account_page');
}

function disable_non_admin_create_account() {
	if(!ossn_isAdminLoggedin()) return false;
}

function disable_create_account_page($dont, $allow, $create, $params) {
	if (ossn_isLoggedin()) return;
	if (strtolower($params['handler']) === 'index') {
		header('Location: '.SITE_URL_LOGIN);
	}
}

ossn_register_callback('ossn', 'init', 'disable_cr_acc_component');
